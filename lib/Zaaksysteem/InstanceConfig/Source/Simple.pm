package Zaaksysteem::InstanceConfig::Source::Simple;

use Moose;

=head1 NAME

Zaaksysteem::InstanceConfig::Source::Simple - Stub-like implementation of
L<Zaaksysteem::InstanceConfig::SourceInterface>

=head1 DESCRIPTION

This source implementation gives free reign to inject, inspect and remove
instance configurations.

This is designed to assist in unit-testing of applications using
L<Zaaksysteem::InstanceConfig|instance configurations>, but is also useful for
rapid prototyping of applications, or one-off scripts that have a set instance
configuration.

=head1 ATTRIBUTES

=head2 _instance_config

Holds a C<HashRef> to the currently configured instance configurations.

=head3 Methods

=over 4

=item add_instance_config

Adds an instance configuration, delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/set($key => $value, $key2 => $value2...)>.

=item find_instance_config_by_hostname

Retrieve an instance configuration by key, delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/get($key, $key2, $key3...)>.

=item has_instance_config

Predicate for the existance of a specific key, delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/exists($key)>.

=item all_instance_configs

Retrieve all instance configurations as a list of values, delegate of
L<Moose::Meta::Attribute::Native::Trait::Hash/values>.

=back

=cut

has _instance_config => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return {}; },
    handles => {
        add_instance_config => 'set',
        find_instance_config_by_hostname => 'get',
        all_instance_hosts => 'keys',
        has_instance_config => 'exists',
        all_instance_configs => 'values'
    }
);

# Apply interface role after setting up _instance_configs so the required
# method 'find_instance_config_by_hostname' exists.
with 'Zaaksysteem::InstanceConfig::SourceInterface';

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
