package Zaaksysteem::InstanceConfig::Model;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints qw[role_type];
use File::Basename qw[dirname];
use File::Spec::Functions qw[catfile];
use Module::Load qw[load];
use Config::General;

use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::InstanceConfig::Model - Configuration (customer_d) model

=head1 DESCRIPTION

    my $model = Zaaksysteem::InstanceConfig::Model->new(
        zaaksysteem_config_path => '/my/path/to/zaaksysteem.conf',
        # Or
        config_source => $my_source
    );

=head1 ATTRIBUTES

=head2 config_source

Reference to an object that implements
L<Zaaksysteem::InstanceConfig::SourceInterface>.

=cut

has config_source => (
    is => 'ro',
    isa => role_type('Zaaksysteem::InstanceConfig::SourceInterface'),
    lazy => 1,
    builder => 'build_config_source',
    handles => {
        'all_instance_hosts' => 'all_instance_hosts'
    }
);

=head2 zaaksysteem_config_path

Path to the main C<zaaksysteem.conf>, which is used to initialize a
L</config_source>.

=cut

has zaaksysteem_config_path => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_zaaksysteem_config_path'
);

=head2 zaaksysteem_config

Parsed datastructure representing the configuration file pointed to by
L</zaaksysteem_config_path>.

=cut

has zaaksysteem_config => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => 'build_zaaksysteem_config'
);

=head2 instance_config_cache

Caching datastructure for instance configurations.

=head3 Delegates

=over 4

=item is_instance_config_cached

=item cache_instance_config

=item get_cached_instance_config

=item clear_cached_instance_configs

=item delete_cached_instance_configs

=back

=cut

has instance_config_cache => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return {}; },
    handles => {
        is_instance_config_cached => 'exists',
        cache_instance_config => 'set',
        get_cached_instance_config => 'get',
        clear_cached_instance_configs => 'clear',
        delete_cached_instance_config => 'delete'
    }
);

=head1 METHODS

=head2 flush

Clear the instance configuration cache(s). After flushing, accessing a
specific instance config will trigger a fetch from the source layer. Use this
behaviour for run-time updating of instance config definitions.

Optionally takes a specific hostname to clear.

=cut

sig flush => '?Str';

sub flush {
    my $self = shift;
    my $host = shift;

    $self->log->info('Flushing instance config cache');

    if (defined $host) {
        $self->delete_cached_instance_config($host);
    } else {
        $self->clear_cached_instance_configs;
    }

    return;
}

=head2 get_instance_config

Retrieve a (cached) instance configuration by hostname.

    my $config = $model->get_instance_config('sprint.zaaksysteem.nl');

=cut

sig get_instance_config => 'Str';

sub get_instance_config {
    my $self = shift;
    my $host = shift;

    $self->log->trace(sprintf(
        'Instance configuration retrieval for "%s"',
        $host
    ));

    if ($self->is_instance_config_cached($host)) {
        return $self->get_cached_instance_config($host);
    }

    $self->log->trace(sprintf(
        'Instance configuration cache-miss for "%s"',
        $host
    ));

    my $config;
    try {
        $config = $self->config_source->find_instance_config_by_hostname($host);
    } catch {
        $self->log->warn(sprintf(
            'Exception caught while finding instance config for host "%s": %s',
            $host,
            $_
        ));
    };

    unless (defined $config) {
        throw('instance_config/host_not_found', sprintf(
            'Failed to retrieve configuration for host "%s"',
            $host
        ));
    }

    $self->cache_instance_config($host, $config);

    return $config;
}

=head2 get_connect_info

Retrieves a datastructure with connection info for the instance's configured
database.

=cut

sig get_connect_info => 'Str => HashRef';

sub get_connect_info {
    my $self = shift;

    my $instance = $self->get_instance_config(shift);

    return $instance->{ 'Model::DB' }{ connect_info };
}

=head2 get_instance_config_config

Helper method for retrieving the configuration of the instance config source
package.

=cut

sub get_instance_config_config {
    my $self = shift;

    my $config = $self->zaaksysteem_config->{ InstanceConfig };

    return $config if defined $config;

    my $config_path = $self->zaaksysteem_config->{ config_directory } // '/etc/zaaksysteem';

    $self->log->trace("Using config_directory $config_path");

    unless (-d $config_path) {
        throw('instance_config/init/config_directory/not_found', sprintf(
            "Config directory %s defined but not found on the filesystem", $config_path
        ));
    }

    # Backwards compatible for previous implementation of instance configurations
    return {
        source_implementation => 'Zaaksysteem::InstanceConfig::Source::Filesystem',
        args => {
            path => catfile($config_path, 'customer.d')
        }
    }
}

=head2 build_config_source

Builder for L</config_source>, if not provided via C<< ->new >>.

=cut

define_profile build_config_source => (
    required => {
        source_implementation => 'Str',
    },
    optional => {
        args => 'HashRef'
    }
);

sub build_config_source {
    my $self = shift;

    my $config = assert_profile($self->get_instance_config_config)->valid;

    load $config->{ source_implementation };

    return try {
        $config->{ source_implementation }->new($config->{ args } || {});
    } catch {
        warn $_;

        return;
    };
}

=head2 build_zaaksysteem_config

Builder for L</zaaksysteem_config>, if not provided via C<< ->new >>.

This method will attempt to load the main configuration file pointed to
L</zaaksysteem_config_path> so it can be used to initialize the config source.

=cut

sub build_zaaksysteem_config {
    my $self = shift;

    unless ($self->has_zaaksysteem_config_path) {
        throw('instance_config/zaaksysteem_conf/path_undefined', sprintf(
            'Cannot build main configuration without path to zaaksysteem.conf'
        ));
    }

    my $path = $self->zaaksysteem_config_path;

    unless (-f $path && -r $path) {
        throw('instance_config/zaaksysteem_conf/path_not_accessible', sprintf(
            'Cannot load configuration file "%s", does not exist or not readable',
            $path
        ));
    }

    my $config = Config::General->new(
        -ConfigFile => $path,
        -ForceArray => 1
    );

    return { $config->getall };
}

=head2 BUILD

Constructor of this object. Will load the necessary configuration source.

=cut

sub BUILD {
    my $self    = shift;

    ### Preload certain attributes
    $self->config_source;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
